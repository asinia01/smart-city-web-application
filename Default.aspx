﻿    <!--Andrew M. Sinarski
        the dude abides -->
<%@ Page Language="C#" async="true" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
 <%
        /***************************************************************
         * copied from https://msdn.microsoft.com/en-us/library/jj819168.aspx
         * necessary for json object
         * 
         ***************************************************************/ 
        Uri geocodeRequest = new Uri("http://dev.virtualearth.net/REST/v1/Traffic/Incidents/42.847542,-84.009212,43.138397,-83.396471?key=AomlDtFLXT-oSngZnuneogR_hphTSuPAl4eSALNIw-OQ_ZvVw4XEMPnkjxAcvvfv");
        GetResponse(geocodeRequest, (x) =>
        {
            Console.WriteLine(x.ResourceSets[0].Resources.Length + " result(s) found.");
           Console.ReadLine();
        });
     %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="refresh" content="60" />
    

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet"/>
    <style>
        table {

            font-family: 'Exo', sans-serif;

        }

    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/ju-1.11.4/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/ju-1.11.4/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-html5-1.2.2/b-print-1.2.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>

    <!--The entire project is 897 lines of code not including the DataContracts class, which I just copied from MSDN.
          That seems incredibly low considering how many hours I've spent on it  -->

    <!-- Reference to the Bing Maps SDK -->
    <script type='text/javascript'
            src='http://www.bing.com/api/maps/mapcontrol?callback=GetMap' 
            ></script>
    
    <script type='text/javascript'>
        
        //declare global arrays to hold number of bikes from code behind
        //and json object
        var res = [];
        var jsonResult = [];
        var information = [];
        var map1;
        var tableData = [];
        var table;
        var table2;
       
        $(document).ready(function () {
            <% var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();%>
            var hvalue = <%= serializer.Serialize(bikesTraffic) %>;
            var hvalueString = hvalue.toString();
            //if there's an emergency, hvalue has a value of 0
            if(hvalueString == '0'){
                for(var i = 0; i < 11; i++){
                    res[i] = "Due to an emergency situation, there are no";
                }
            }
            else{
                res = hvalueString.split(",");
            }

            $('#trafficBox').on('click',function(){
                var id = $(this).find(":selected").attr('id');
                var cords = id.split(":");
                var y = cords[0];
                var x = cords[1];
              
                var info = getResourceInformation(x,y);
                alert(info); 
            });

            table = $('#example').DataTable( {
                data: tableData,
                "columnDefs": {
                    className: "dt-head-center"
                }
            } );

            table2 = $('#example2').DataTable( {
                data: tableData,
                "columnDefs": {
                    className: "dt-head-center"
                }
            } );           
        });

        function buildTrafficReport(){

            getTrafficInformation();
            //credited to Ashton Bradley for assistance with this function    
        }

        function columnColor(content){
            return '<span style="color:#1c94c4;">'+content+'</span>';
        }

        function getTrafficInformation(){
            $.ajax({
                method: "GET",
                url: "http://dev.virtualearth.net/REST/v1/Traffic/Incidents/37,-105,45,-94?key=AomlDtFLXT-oSngZnuneogR_hphTSuPAl4eSALNIw-OQ_ZvVw4XEMPnkjxAcvvfv"
                //dataType: "jsonp"
            })
          .done(function( data ) {
              information = data;
              console.log(data);
              var pos = 0;
              var statusColor;
              
              for(var y = 0 ; y <  information['resourceSets'].length; y++) {
                  for(var x = 0 ; x < information['resourceSets'][y].resources.length; x++ ){
                  
                      var severity = information['resourceSets'][y].resources[x].severity;
                      if (severity == 1 || severity == 2) {
                          statusColor = '<div class="green" style="color: green;border-radius:10px;width:20px;height:20px;border: 1px solid;background-color: green;padding: 6px;"></div>';
                      }
                      else if (severity == 3){
                          statusColor = '<div class="yellow" style="color: yellow;border-radius:10px;width:20px;height:20px;border: 1px solid;background-color: yellow;padding: 6px;"></div>';
                      }
                      else{
                          statusColor = '<div class="red" style="color: red;border-radius:10px;width:20px;height:20px;border: 1px solid;background-color: red;padding: 6px;"></div>';
                      }
                      $('#trafficBox').append('<option id="'+y+':'+x+'">'+ information['resourceSets'][y].resources[x].description+'</option>');

                      tableData[pos] = [information['resourceSets'][y].resources[x].description ,information['resourceSets'][y].resources[x].point.coordinates[0] , information['resourceSets'][y].resources[x].point.coordinates[1] , information['resourceSets'][y].resources[x].toPoint.coordinates[0], information['resourceSets'][y].resources[x].toPoint.coordinates[1]];
                      table.row.add([columnColor(information['resourceSets'][y].resources[x].incidentId), columnColor(information['resourceSets'][y].resources[x].description) ,coordColor(information['resourceSets'][y].resources[x].point.coordinates[0]) , coordColor(information['resourceSets'][y].resources[x].point.coordinates[1]) , coordColor(information['resourceSets'][y].resources[x].toPoint.coordinates[0]), coordColor(information['resourceSets'][y].resources[x].toPoint.coordinates[1]), statusColor]).draw( false );
                          pos++;

                      console.log(information['resourceSets'][y].resources[x]);
                      var location = new Microsoft.Maps.Location(information['resourceSets'][y].resources[x].point.coordinates[0], information['resourceSets'][y].resources[x].point.coordinates[1]);
                      var pushpin = new Microsoft.Maps.Pushpin(location);
                      pushpin.metadata = {
                          title: information['resourceSets'][y].resources[x].description,
                          description: information['resourceSets'][y].resources[x].description
                      };

                      map1.entities.push(pushpin);
                      pushpin.setOptions({ text:information['resourceSets'][y].resources[x].description, title:information['resourceSets'][y].resources[x].description, subTitle:information['resourceSets'][y].resources[x].description, color: 'blue', enableHoverStyle: true, enableClickedStyle: true});
               
                  }
              }

          });
         
        }


        function getResourceInformation(x , y ){
            return information['resourceSets'][y].resources[x];
        }

        function coordColor(coord){
            var color = "green";
            if(coord < 0){
                color="red";
            }

            return "<span style='color:"+color+";'>"+coord+"</span>";
        }
        /***************************************
        *
        *       Load the map  and pushpins
        *
        ******************************************/
    function GetMap()
    {
        //create the map, includes my Bing map key
         map1 = new Microsoft.Maps.Map('#myMap',
        {
            credentials: 'AomlDtFLXT-oSngZnuneogR_hphTSuPAl4eSALNIw-OQ_ZvVw4XEMPnkjxAcvvfv',
            center: new Microsoft.Maps.Location(43.016193, -83.705521),
            mapTypeId: Microsoft.Maps.MapTypeId.canvasLight,
            zoom: 12        
        });
        /********************************************************
        *
        *Traffic Module code found at https://msdn.microsoft.com/en-us/library/mt750540.aspx
        *
        *
        *******************************************************/
        Microsoft.Maps.loadModule('Microsoft.Maps.Traffic', function () {
            var manager = new Microsoft.Maps.Traffic.TrafficManager(map1);
            manager.show();
        });
        //create array to hold locations of pushpins
        var location = [];
        location[0] = map1.getCenter();
        location[1] = new Microsoft.Maps.Location(43.027516, -83.727318);
        location[2] = new Microsoft.Maps.Location(42.990869, -83.662097);
        location[3] = new Microsoft.Maps.Location(42.987110, -83.710585);
        location[4] = new Microsoft.Maps.Location(43.006086, -83.682275);
        location[5] = new Microsoft.Maps.Location(42.967708, -83.745224);
        location[6] = new Microsoft.Maps.Location(43.048270, -83.648694);
        location[7] = new Microsoft.Maps.Location(43.018316, -83.667818);
        location[8] = new Microsoft.Maps.Location(43.021721, -83.701884);
        location[9] = new Microsoft.Maps.Location(42.988807, -83.670760);

        //declare arrays to hold pin data
        var pinLabel = [];
        var pinTitle = [];
        var pinSubTitle = [];
        var pinColor = [];

        //load pin data
        for (var i = 0; i < location.length; i++) {
            pinLabel[i] = (i + 1).toString();
            pinTitle[i] = 'Bike Depot';
            if(i == 0) { //this is special for city center pin
                pinSubTitle[i] = 'City Center';
                pinColor[i] = 'red';
                table2.row.add([columnColor(pinTitle[i])  ,coordColor(location[i].latitude) , coordColor(location[i].longitude), res[i] , '<div class="red" style="color: red;border-radius:10px;width:20px;height:20px;border: 1px solid;background-color:red;padding: 6px;"></div>']).draw( false );
    
            }
            else {  //all other pins
                pinSubTitle[i] = '';
                pinColor[i] = 'purple';
                table2.row.add([columnColor(pinTitle[i]) ,coordColor(location[i].latitude) , coordColor(location[i].longitude) , res[i] , '<div class="purple" style="color: purple;border-radius:10px;width:20px;height:20px;border: 1px solid;background-color:purple;padding: 6px;"></div>']).draw( false );
            }                       
        }
 
        //declare and instantiate infobox
       var infobox = new Microsoft.Maps.Infobox(map1.getCenter(), { visible: false });
        infobox.setMap(map1);

        //declare pins
        for (var i = 0; i < location.length; i++) {
            var pushpin = new Microsoft.Maps.Pushpin(location[i]);

            //metadata for pins
            if(i == 0)  //first pin is City Center
            {
                pushpin.metadata = {
                    title: 'City Center',
                    description: res[i] + ' bikes available.'
                };
            }
            else
            {
                pushpin.metadata = {
                        title: 'Bike Depot ' + (i + 1),
                        description: res[i] + ' bikes available.'
                };
            }

            //add handler for pins
            Microsoft.Maps.Events.addHandler(pushpin, 'click', function(args) {
                infobox.setOptions({
                    location: args.target.getLocation(),
                    title: args.target.metadata.title,
                    description: args.target.metadata.description,
                    visible: true
                });
            });

            Microsoft.Maps.Events.addHandler(pushpin, 'mouseout', function(args) {
                infobox.setOptions({
                    location: args.target.getLocation(),
                    title: args.target.metadata.title,
                    description: args.target.metadata.description,
                    visible: false
                });
                pushpin.setOptions({enableHoverStyle: false, enableClickedStyle: false});
            });
            
            //instantiate pins
            
            //set text, title, subtitle, color and styles to pins
            pushpin.setOptions({ text: pinLabel[i], title: pinTitle[i], subTitle: pinSubTitle[i], color: pinColor[i], enableHoverStyle: true, enableClickedStyle: true});
            map1.entities.push(pushpin);

        }     
        buildTrafficReport();
        
    }


    </script>
</head>
<body style="width: 1076px;overflow-x: hidden;">

<div id="main-container" style="min-width: 1077px;max-width: 1077px;">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hiddenField" runat="server" />
    <div id="myMap"style="position:relative;width:600px;height:400px;float:left"></div>
        <div id="flintImage" style="position:relative;float:right">
            <asp:Image ID="imgFlint" runat="server" Height="415px" ImageUrl="~/Images/realFlint.png" Width="476px" />
        </div>
<div style="width:100%">

    <span id="MySpan" runat="server"  ><asp:TextBox ID="txtDisplay" runat="server" Font-Bold="True" ForeColor="Red" ReadOnly="True" Width="595px" BackColor="White"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Font-Size="Large" Text="Traffic Data"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;

</div>
<div style="width:100%">

    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Incident ID</th>
                <th>Description</th>
                <th>FromX</th>
                <th>FromY</th>
                <th>ToX</th>
                <th>ToY</th>
                <th>Status</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Incident ID</th>
                <th>Description</th>
                <th>FromX</th>
                <th>FromY</th>
                <th>ToX</th>
                <th>ToY</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>


</div>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/bike2.jpg" Width="190px" Height="182px" />
        <div style="width:100%; padding-top: 26px;">

    <table id="example2" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Description</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Bikes</th>
                <th>Status</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Description</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Bikes</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>


</div>
     
         <!--<asp:TextBox ID="txtBoxTraffic" runat="server" Height="167px" ReadOnly="True" style="z-index: 1" Width="552px"></asp:TextBox>-->
         <!--<asp:ListBox ID="trafficBox" runat="server" Height="167px" ReadOnly="True" style="z-index: 1" Width="552px"></asp:ListBox>-->

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:TextBox ID="txtEmergency" runat="server" ForeColor="Red" Height="167px" Visible="False" Width="559px" BackColor="White" Font-Bold="True" Font-Size="Large" style="z-index: -1"></asp:TextBox>
        <br />
        <asp:Image ID="imgEmergency" runat="server" Height="230px" ImageUrl="~/Images/emergency.png" Visible="False" Width="264px" style="z-index: -1" />
        <asp:Image ID="imgBike" runat="server" ImageUrl="~/Images/bike2.jpg" Width="137px" />
        &nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblEmergency" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="Red" Text="Emergency Situation" Visible="False"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
&nbsp;&nbsp;&nbsp;&nbsp; </span>
        <br />
        <br />
        
    </form>
</div>
</body>
</html>
