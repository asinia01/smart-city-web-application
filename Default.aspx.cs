﻿//Programmer: Andrew M. Siniarski
//            the dude abides
//Program:    code behind file for default.aspx
//Date:       30 Oct 2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.ComponentModel;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using BingMapsRESTService.Common.JSON;

public partial class _Default : System.Web.UI.Page
{

    //class variables
    //initialized to 0 for emergency situation purposes
    protected int[] bikesTraffic = { 0 };
    protected void Page_Load(object sender, EventArgs e)
    {
        //color the background
        form1.Attributes.Add("style", "background-color:#699fe5");

        //declare necessarry variables
        string emergencyString;

        //instantiate bus and bike objects
        BusLine bus = new BusLine();
        BikeDepot bike = new BikeDepot();

        //declare and instantiate threads for objects
        Thread bikeThread = new Thread(new ThreadStart(bike.checkTraffic));
        Thread busThread = new Thread(new ThreadStart(bus.checkTraffic));

        //start and join threads
        bikeThread.Start();
        busThread.Start();
        bikeThread.Join();
        busThread.Join();

        /***************************************
         * 
         *          Check for emergency
         * 
         * *************************************/
        if (bus.emergencyFlag || bike.emergencyFlag)
        {
            emergencyString = bus.emergencySituation(); //get the emergency string
            //set appropriate fields to visible or not visible
            txtDisplay.Visible = false;
            imgBike.Visible = false;
            imgEmergency.Visible = true;
            lblEmergency.Visible = true;
            txtEmergency.Visible = true;
            txtEmergency.Text = emergencyString;

        }
        else
        {
            /****************************************
             * 
             *              Bike Stuff
             * 
             ****************************************/
            if (bike.flag)     //there's traffic
            {
                bikesTraffic = bike.intBikesUsedTraffic();
            }
            else    //there's no traffic
            {
                bikesTraffic = bike.intBikesUsedNoTraffic();
            }

            /****************************************
             * 
             *              Bus Stuff
             * 
             ****************************************/
            if (bus.flag)      // there's traffic
            {
                txtDisplay.Text = "There is currently a slowdown on all freeways and highways";
            }
            else      //no traffic
            {
                txtDisplay.Text = "There is currently no delay";
            }
        }
    }

    /***************************************************************
         * copied from https://msdn.microsoft.com/en-us/library/jj819168.aspx
         * necessary for json object
         * 
         ***************************************************************/
    public void GetResponse(Uri uri, Action<Response> callback)
    {
        WebClient wc = new WebClient();
        wc.OpenReadCompleted += (o, a) =>
        {
            if (callback != null)
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Response));
                 callback(ser.ReadObject(a.Result) as Response);
            }
        };
        
    }
}
