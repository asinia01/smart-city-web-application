﻿//Programmer:   Andrew M. Siniarski
//              the dude abides
//Date:         26 Oct 2016
//Purpose:      class for the bike depot


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Diagnostics;
using System.Text;


public partial class BikeDepot: System.Web.UI.Page
{

    //DECLARE constant for probability of an emergency situation
    //and constant for random traffic slow down
    private const int EMERGENCY = 2;
    private const int RANDOM_TRAFFIC = 10;
    private const int BIKES_PER_DEPOT = 25;

    //declare and initialize appropriate variables
    public bool flag = false;   //this is a flag that can be accessed on the code behind page
    public bool emergencyFlag = false;   //this is a flag that can be accessed on the code behind page 
    private string usedBikeString = "The number of bicycles available at bike depot number ";
    


    /*****************************************
    * 
    * Constructor
    * 
    * *********************************////
    public BikeDepot()
    {
        
    }

    /**********************************************
     * 
     * End Constructor
     * Start class methods
     * 
     *  *************************************///


    //method to check traffic
    //it will call getTime() method
    //then call getRandom() to determine if there's a slow down
    public void checkTraffic()
    {

        int random = getRandom();

        //if the random is 1 or 2
        if (random <= EMERGENCY)
        {
            emergencyFlag = true;
            emergencySituation();
        }

        double hourFromMidnight = getTime();

        /************************************
         * 
         * start the check for traffic AM/PM
         * 
         * ***********************************/
        //5am to 6am  or 2pm to 3pm
        if ((hourFromMidnight >= 5 && hourFromMidnight < 6) || (hourFromMidnight >= 14 && hourFromMidnight < 15))
        {
            if (random < 25)
            {
                flag = true;
            }
        }
        //6am to 7am or 3pm to 4pm
        else if ((hourFromMidnight >= 6 && hourFromMidnight < 7) || (hourFromMidnight >= 15 && hourFromMidnight < 16))
        {
            if (random < 40)
            {
                flag = true;
            }
        }
        //7am to 8am or 4pm to 5pm
        else if ((hourFromMidnight >= 7 && hourFromMidnight < 8) || (hourFromMidnight >= 16 && hourFromMidnight < 17))
        {
            if (random < 50)
            {
                flag = true;
            }
        }
        //8am to 9am or 5pm to 6pm
        else if ((hourFromMidnight >= 8 && hourFromMidnight < 9) || (hourFromMidnight >= 17 && hourFromMidnight < 18))
        {
            if (random < 35)
            {
                flag = true;
            }
        }
        //chance of random traffic
        else if (random <= RANDOM_TRAFFIC)
        {
            flag = true;
        }
    }
    //*********************** end getTraffic()

    //method to get random number
    //in a method to make sure that it's as random as possible
    public int getRandom()
    {
        Random random = new Random();
        int randomNumber = random.Next(1, 101);
        return randomNumber;
    }
    //***************************** end getRandom

    //method to retrieve and return the number of hours from midnight
    public double getTime()
    {
        //get the TimeSpan from midnight
        TimeSpan fromMidnight = DateTime.Now.TimeOfDay;
        //get the number of hours from midnight
        double hour = fromMidnight.TotalHours;
        return hour;
    }
    //********************************************end getTime()

    //method to return emergency situation string
    public string emergencySituation()
    {
       return Emergency.getEmergency();
    }
    //**********end emergency situation

    //************************* return string for number of bikes with traffic
    public string numberUsedBikesTraffic()
    {
        StringBuilder str = new StringBuilder();
        Random bikeRandom = new Random();
        int[] bikesUsedArray = intBikesUsedTraffic();

        for (int i = 1; i < 11; i++)
        {
            //NumBikes = BIKES_PER_DEPOT - bikeRandom.Next(1, 26);
            str.Append(usedBikeString + i + " are " + bikesUsedArray[i-1] + "\r\n");
        }
        return str.ToString();
    }
    //******************end numberUsedBikesTraffic

    //*************************return string for number of bikes with no traffic
    public string numberUsedBikesNoTraffic()
    {

        int[] bikesUsedAray = intBikesUsedNoTraffic();
        StringBuilder str = new StringBuilder();
        
        for (int i = 1; i < 11; i++)
        {
            str.Append(usedBikeString + i + " are " + bikesUsedAray[i-1] + "\r\n");    
        }

        return str.ToString();
    }
    //******************end numberBikesUsedNoTraffic

    //*********return an array of bikes used  TRAFFIC
    //******** added to simplify the javascript for aspx page
    public int [] intBikesUsedTraffic()
    {
        Random bikeRandom = new Random();
        int[] bikesUsedArray = new int[10];

        for(int i = 0; i < 10; i++)
        {
            bikesUsedArray[i] = BIKES_PER_DEPOT - bikeRandom.Next(1, 26);
        }

        return bikesUsedArray;
    }
    //*****************end bikesUsedTraffic

    //***************return an array of bikes used NO TRAFFIC
    //**************loads BIKES_PER_DEPOT const into array
    public int [] intBikesUsedNoTraffic()
    {
        int[] bikesUsedArray = new int[10];

        for(int i = 0; i < 10; i++)
        {
            bikesUsedArray[i] = BIKES_PER_DEPOT;
        }

        return bikesUsedArray;
    }
}