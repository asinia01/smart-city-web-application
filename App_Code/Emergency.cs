﻿//Programmer: Andrew M. Siniarski
//            the dude abides
//Date:       November 2016
//Purpose:    class to handle emergency situations

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Emergency
/// </summary>
public class Emergency : System.Web.UI.Page
{
    //array to hold strings of emergency situations
    private static string[] emergencyArray = new string[]
        {"There has been a chemical spill on Dort Highway (M-54)",
         "There is a terrorist situation at Bishop Airport",
         "There is a chemical fire at General Motors Flint Engine Operations",
         "Robert T Longway is closed due to a police situation",
         "I-69 is closed due to a car fire",
         "I-75/US-23 is closed due to an airplane on the freeway",
         "A large fleet of pterodactyls is descending on the downtown area and ripping the citizens limb from limb"}; //thanks to Alex Brouwer for the pterodactyls

    //public string to hold the emergency situation
    public static string emergencyString = "";

    public Emergency()
    {
       
    }

    //method to get random number
    //in a method to make sure that it's as random as possible
    public static int getRandom()
    {
        Random random = new Random();
        int randomNumber = random.Next(0, 7);

        return randomNumber;
    }

    //method to randomly assign an emergency
    public static string getEmergency()
    {
        int randomNumber = getRandom();
        emergencyString = emergencyArray[randomNumber];
        return emergencyString;
    }
}